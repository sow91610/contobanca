//LIBRERIE
import java.text.DateFormat;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
import java.util.Calendar;  
import java.util.ArrayList; 
import java.util.Scanner;
public class ContoCorrente {

// ATTRIBUTI
public Persona intestatario = new Persona("Frank", "Stone", 26, "Unknown Avenue", "3547469446");
public double saldo;
ArrayList<Movimento> movimenti = new ArrayList<Movimento>();
public int index = 0;
private boolean bloccato = false;  
private boolean fidoAttivo = false;

// COSTRUTTORE
public ContoCorrente(double saldoIniz,  int numMovimenti){
  this.saldo = saldoIniz;
  this.movimenti = movimenti;
  this.intestatario = intestatario;
}

 public void getSaldo(){
   if(bloccato){
     System.out.println("Conto bloccato");   
   }
   else{
    System.out.println("Il saldo attuale è: " + this.saldo);
   }
  }

  public void datiProprietario(){
    System.out.println(this.intestatario.toString());
  }

  public void printAll(){
    for (int i = 0; i<movimenti.size(); i++){
      System.out.println("Importo: " + movimenti.get(i).importo);
      System.out.println("Data richiesta: " + movimenti.get(i).dataRichiesta);
      System.out.println("Causale: " + movimenti.get(i).causale);
      System.out.println("Data valuta: " + movimenti.get(i).dataValuta);
      System.out.println("Tipologia: " + movimenti.get(i).tipologia);
    }
  }


  //BONIFICO
  public double bonifico( double sommaBonifico){
    
    if(bloccato){
      System.out.println("Conto bloccato");
      return 0.0;
    }
    else{   
      Date date = Calendar.getInstance().getTime();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
      String strDate = dateFormat.format(date);
        Scanner scanner = new Scanner(System.in);
      System.out.println("Inserire causale movimento: ");
       String causale = scanner.nextLine();
      movimenti.add(new 
       Movimento("Bonifico",sommaBonifico,strDate,causale,strDate)) ; 
    this.saldo+= movimenti.get(index).importo;
      index++;
    return this.saldo;
    }
  }


  //PRELIEVO
  public double prelievo( double sommaDaPagare){
    if(bloccato ){
      System.out.println("Conto bloccato");
      return 0.0;
    }
    else{            
      Date date = Calendar.getInstance().getTime();
      DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
      String strDate = dateFormat.format(date);
      Scanner scanner = new Scanner(System.in);
      System.out.println("Inserire causale movimento: ");
      String causale = scanner.nextLine();
      movimenti.add(new 
      Movimento("Prelievo",sommaDaPagare,strDate,causale,strDate)); 
      
    
        this.saldo = saldo - movimenti.get(index).importo;
        index++;
        return this.saldo;
        
      }
      
  
    }
  }