public class Persona {

private String nome;
private String cognome;
private int eta;
private String indirizzo;
private String telefono;
  

public Persona(String nome, String cognome, int eta, String indirizzo, String telefono) {
    this.nome = nome;
    this.cognome = cognome;
    this.eta = eta;
    this.indirizzo = indirizzo;
    this.telefono = telefono;
}

  public String toString(){
    return "Nome: " + nome + " Cognome: " + cognome + " Eta: " + eta + 
      " Indirizzo: " + indirizzo + " Telefono: " + telefono;
  }


}