public class Movimento {
  public double importo;
  public String dataRichiesta;
  public String causale;
  public String dataValuta;
  public String tipologia;

  public Movimento(String tipologia, double importo, String data, String causale, String dataValuta){
    this.tipologia = tipologia;
    this.importo = importo;
    this.dataRichiesta = data;
    this.causale = causale;
    this.dataValuta = dataValuta;
    
  }
  
}

